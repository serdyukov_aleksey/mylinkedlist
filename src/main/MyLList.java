

public class MyLList implements IList {

    private Node root;
    private Node last;

    private static class Node {
        int element;
        Node next;
        Node prev;
        Node(int element){
            this.element = element;
        }
    }

    @Override
    public void clear() {
        this.root = null;
    }

    @Override
    public int size() {
        Counter counter = new Counter();
        return sizeM(this.root,counter);

    }

    private int sizeM(Node node, Counter counter){
        if (node == null) {
            return counter.index;
        }else {
            sizeM(node.next, counter);
            counter.index++;
        }
        return counter.index;
    }

    @Override
    public int get(int index) {
        if (index < 0 || index >= size()){
            return 0;
        }
        Node result = root;
        for (int i = 0; i < index; i++) {
            result = result.next;
        }
        return result.element;
    }

    @Override
    public boolean add(int number) {
        Node node = new Node(number);
        if (this.root == null){
            node.next = null;
            node.prev = null;
            root = node;
            last = node;
        }else {
            last.next = node;
            node.prev = last;
            last = node;
        }
        return false;
    }

    @Override
    public boolean add(int index, int number) {
        int size = size();
        Node old = this.root;
        this.root = null;
        if (index < 0 || index >= size){
            return false;
        }
        for (int i = 0; i < size; i++) {
            if (index == i){
                add(number);
            }
            add(old.element);
            old = old.next;
        }
        return true;
    }

    @Override
    public int remove(int number) {
        int size = size();
        Node old = this.root;
        this.root = null;
        for (int i = 0; i < size; i++){
            if (old.element != number){
                add(old.element);
            }
            old = old.next;
        }
        return 0;
    }

    @Override
    public int removeByIndex(int index) {
        int size = size();
        Node old = this.root;
        this.root = null;
        for (int i = 0; i < size; i++){
            if (index != i){
                add(old.element);
            }
            old = old.next;
        }
        return 0;
    }

    @Override
    public boolean contains(int number) {
        int size = size();
        Node node = root;
        for (int i = 0; i < size; i++){
            if (number == node.element){
                return true;
            }
            node = node.next;
        }
        return false;
    }

    @Override
    public boolean set(int index, int number) {
        int size = size();
        if (index < 0 || index >= size){
            return false;
        }
        Node node = root;
        for (int i = 0; i < index; i++){
            node = node.next;
        }
        node.element = number;
        return true;
    }

    @Override
    public void print() {
        Node node = root;
        for (int i = 0; i < size(); i++){
            System.out.print(node.element + " ");
            node = node.next;
        }
        System.out.println();
    }

    @Override
    public int[] toArray() {
        int size = size();
        int[] arr = new int[size];
        Node node = root;
        for (int i = 0; i < size; i++){
            arr[i] = node.element;
            node = node.next;
        }
        return arr;
    }

    @Override
    public int[] subList(int fromIdex, int toIndex) {
        int[] arr = new int[toIndex - fromIdex];
        Node node = root;
        for (int i = 0; i < toIndex; i++){
            if (i >= fromIdex){
                arr[i - fromIdex] = node.element;
            }
            node = node.next;
        }
        return arr;
    }

    @Override
    public boolean removeAll(int[] ar) {
        Node node = root;
        for (int i : ar){
            if(contains(i)){
                remove(i);
            }
        }
        return true;
    }

    @Override
    public boolean retainAll(int[] ar) {
        Node node = root;
        boolean check = false;
        int size = size();
        for (int j = 0; j < size; j++) {
            for (int i : ar) {
                if (node.element == i) {
                    check = true;
                    break;
                }
            }
            if (!check){
                remove(node.element);
            }
            check = false;
            node = node.next;
        }
        return true;
    }

    private class Counter {
        int index = 0;
    }
}