

public interface IList {

    void clear();
    int size();
    int get(int index);
    boolean add(int number);
    boolean add(int index, int number);
    int remove(int number);
    int removeByIndex(int index);
    boolean contains(int number);
    boolean set(int index, int number);
    void print();
    int[] toArray();
    int[] subList(int fromIdex, int toIndex);
    boolean removeAll(int[] ar);
    boolean retainAll(int[] ar);

}
