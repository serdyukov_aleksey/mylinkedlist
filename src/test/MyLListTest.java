import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MyLListTest {

    private MyLList myLList;
    private final int[] ARR_CHECK = {4,5,8,100,7};

    MyLListTest(){
        myLList = new MyLList();
    }

    @BeforeEach
    void TestL(){
        myLList.add(5);
        myLList.add(6);
        myLList.add(7);
        myLList.add(50);
        myLList.add(100);
        myLList.add(68);
    }

    @Test
    void testSize(){
        int exp = 6;
        int act = myLList.size();
        assertEquals(exp, act);
    }

    @Test
    void testClear(){
        myLList.clear();
        int exp = 0;
        int act = myLList.size();
        assertEquals(exp, act);
    }

    @Test
    void testToArray(){
        int[] exp = {5,6,7,50,100,68};
        int[] act = myLList.toArray();
        assertArrayEquals(exp,act);
    }

    @Test
    void testaddIndex(){
        myLList.add(3,1000);
        int exp1 = 1000;
        int act1 = myLList.get(3);
        int[] exp2 = {5,6,7,1000,50,100,68};
        int[] act2 = myLList.toArray();
        assertEquals(exp1, act1);
        assertArrayEquals(exp2,act2);
    }


    @Test
    void testSubList(){
        int[] exp = {7,50,100};
        int[] act = myLList.subList(2,5);
        assertArrayEquals(exp,act);
    }

    @Test
    void testRemoveAll(){
        myLList.removeAll(ARR_CHECK);
        int[] exp = {6,50,68};
        int[] act = myLList.toArray();
        assertArrayEquals(exp,act);
    }

    @Test
    void testRetainAll(){
        myLList.retainAll(ARR_CHECK);
        int[] exp = {5,7,100};
        int[] act = myLList.toArray();
        assertArrayEquals(exp,act);
    }
}